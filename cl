#!/bin/bash

if [[ $# -ne 1 ]]
then 
	echo "This script requires exactly one file path."
	exit 1
fi

# Colorize log
ccze -A < $1 | less -R
